import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

/**
 * 
 */

/**
 * @author rafaelseara
 *
 */
public class Tests {

	/**
	 * this test aims to read only the available inputs by the user. 
	 * only values from 1 to 5 are correct 
	 * Test method for {@link Main#userInteraction()}.
	 */
	@Test
	public void testUserInteraction() {
		String fileName = Main.userInteraction();
		switch (fileName) {
			case "5x8.json":
				break;
			case "100x100.json":
				break;
			case "1000x1000.json":
				break;
			case "10000x10000.json":
				break;
			case "20000x20000.json":
				break;
			default:
				fail("Bad Input");
		}	    
	}

	/**
	 * This test compares the output of the matrix 5x8 execution to the given output in the quizz
	 * this method can be easy extended to other outputs if given.
	 * Test method for {@link Main#calcAdjacentCells(int[][])}.
	 */
	@Test
	public void testCalcAdjacentCells() {
		
		ArrayList<ArrayList<String>> myOutput5x8 = new ArrayList<ArrayList<String>>();
		ArrayList<String[]> reality5x8 = new ArrayList<String[]>();
		
		String[] o1 = {"[0,3]", "[1,2]", "[1,3]", "[1,4]"};
		String[] o2 = { "[0,6]", "[0,7]", "[1,6]", "[1,7]", "[2,6]", "[3,6]", "[3,7]", "[4,6]", "[4,7]"};
		String[] o3 = {"[3, 3]", "[4,3]"};

		reality5x8.add(o1);
		reality5x8.add(o2);
		reality5x8.add(o3);
		
		myOutput5x8 = Main.calcAdjacentCells(Main.readFile("5x8.json"));
		Iterator<ArrayList<String>> iterator = myOutput5x8.iterator();
		
		if(myOutput5x8.size() == reality5x8.size() ){
			int i = 0;
			while (iterator.hasNext()) {
				String rowOutput = iterator.next().toString();
				String[] parts = rowOutput.split(" ");
				String[] realityParts = reality5x8.get(i);

				if(parts.length == realityParts.length) {
					for( String part:parts) {
						//trim part
						if(part.charAt(0)=='[' && part.charAt(1)=='[') {
							part = part.substring(1);
						}
						if(part.charAt(part.length()-1)==',' || 
							part.charAt(part.length()-1)==']' && part.charAt(part.length()-2)==']'){
								part = part.substring(0, part.length()-1);
						}
						for(int j=0; j<realityParts.length; j++) {

							if(part.equals(realityParts[j])) {
								break;
							}else {
								if(j==realityParts.length) {
									fail("Wrong Output");
								}
							}
						}
					}
				}else {
					fail("Wrong Output row");
				}
				i+=1;
			}
		}else {
			fail("Wrong Output");
		}
	}

	/**
	 * Test method for {@link Main#findAdj(int, int, java.util.ArrayList, int[][], int[][])}.
	 */
	@Test
	public void testFindAdj() {
		fail("Not yet implemented");
	}

	/**
	 * This test verifies if a two dimensional array of zeros is checked.
	 * It's not and it will fill it and check again.
	 * Test method for {@link Main#isNotChecked(int, int, int[][])}.
	 */
	@Test
	public void testIsNotChecked() {
		int[][] checked = new int[5][5];
		for(int i = 0; i<checked.length; i++) {
			for(int j = 0; j<checked[0].length; j++) {
				if(Main.isNotChecked(i, j, checked)) {
					checked[i][j]=1;
				}else {
					fail("Error");
				}
			}
		}
		for(int k = 0; k<checked.length; k++) {
			for(int l = 0; l<checked[0].length; l++) {
				if(Main.isNotChecked(k, l, checked)) {			
					fail("Error");
				}
			}
		}
	}

	/**
	 * 
	 * this test reads the file 5x8.json and verifies it the conversion to a two dimensional
	 * array is good.
	 * Test method for {@link Main#readFile(java.lang.String)}.
	 */
	@Test
	public void testReadFile() {
		
		int[][] matrix5x8 = 	{{0,0,0,1,0,0,1,1},
							 {0,0,1,1,1,0,1,1},
							 {0,0,0,0,0,0,1,0},
							 {0,0,0,1,0,0,1,1},
							 {0,0,0,1,0,0,1,1}};
		
		int[][] readFile = Main.readFile("5x8.json");
		
		for(int i = 0; i<matrix5x8.length; i++) {
			for(int j = 0; j<matrix5x8[0].length; j++) {	
				if(matrix5x8[i][j] != readFile[i][j]) {
					fail("Bad File readings");
				}
			}
				
		}
	}

}
