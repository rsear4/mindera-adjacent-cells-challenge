import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


/**
 * This code was created by Rafael Seara for the Mindera Graduates challenge. 
 * @author rafaelseara 
 * @email r.seara@campus.fct.unl.pt
 *
 */
public class Main {


	//positions that were searched before 0-not searched 1-searched	
	/**
	 * the main class reads the file selected by the user in the console. files 1 to 5 are available
	 * After reading the file the adjacent cells are calculated and printed to the console
	 * @param args
	 */
	public static void main(String[] args) {

		//user interaction from console
		String fileSelection = userInteraction();

		int[][] matrix  = readFile(fileSelection);

		ArrayList<ArrayList<String> > output = new ArrayList<ArrayList<String> >();
		output = calcAdjacentCells(matrix);

		//print the output
		Iterator<ArrayList<String>> iterator = output.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next().toString());
		}
	}

	
	/**
	 * Shows the available files to choose and asks for user input
	 * The user input must be a number from 1 to 5 if no files are added or removed
	 * @return String of selected file
	 */
	public static String userInteraction() {
		System.out.println("select matrix dimension:");
		System.out.println("1 - 5x8");
		System.out.println("2 - 100x100");
		System.out.println("3 - 1000x1000");
		System.out.println("4 - 10000x10000");
		System.out.println("5 - 20000x20000");
		System.out.println();
		System.out.println("Insert Selection:");

		Scanner userScan = new Scanner(System.in);
		int sel = userScan.nextInt();
		String selectedFile = "";
		switch (sel) {
		case 1:
			selectedFile = "5x8.json";
			break;
		case 2:
			selectedFile = "100x100.json";
			break;
		case 3:
			selectedFile = "1000x1000.json";
			break;
		case 4:
			selectedFile = "10000x10000.json";
			break;
		case 5:
			selectedFile = "20000x20000.json";
			break;
		default:
			System.out.println("Introduce an option number");
		}	    
		userScan.close();
		return selectedFile;
	}

	
	
	/**
	 * 
	 * This method finds the adjacent cells with ones in a given matrix. only top, bottom, left and right
	 * positions are searched using the findAdj method.
	 * @param matrix two dimensional with zeros and ones
	 * @return
	 */
	public static ArrayList<ArrayList<String> > calcAdjacentCells(int[][] matrix) {
		
		int rows = matrix.length;
		int columns = matrix[0].length;
		int[][] checked = new int[rows][columns];

		//structure used to store and print the output
		ArrayList<ArrayList<String> > output = new ArrayList<ArrayList<String> >();

		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < columns; j++) {
				if(isNotChecked(i,j, checked)) {
					checked[i][j] = 1;
					if(matrix[i][j] == 1) {
						ArrayList<String> adj = new ArrayList<String>();
						adj.add("["+i+","+j+"]");
						//adjacent cells of i j 
						adj = findAdj(i,j, adj, matrix, checked);

						//only groups bigger than 1 are considered
						if(adj.size() > 1) {
							output.add(adj);
						}
					}
				}
			}
		}

		return output;
	}

	/**
	 * search method for ones.search positions are: top, bottom, left and right positions starts
	 * That's done recursively
	 * @param i current row number
	 * @param j current column number
	 * @param adj list of strings with previous adjacent cells. 
	 * this will increment his size if new valid adjacent cells are found
	 * @param matrix 
	 * @return 
	 * 
	 * @return the list of strings of adjacent cells
	 */
	public static ArrayList<String> findAdj(int i, int j, ArrayList<String> adj, int[][] matrix, int[][] checked){
		try {
			//check top
			if(isNotChecked(i-1,j, checked)) {
				checked[i-1][j] = 1;
				if(matrix[i-1][j]==1) {
					adj.add("["+(i-1)+","+j+"]");
					findAdj(i-1,j, adj, matrix, checked);
				}
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
			//search position outside of matrix
		}
		try {
			//check bottom
			if(isNotChecked(i+1,j, checked)) {
				checked[i+1][j] = 1;
				if(matrix[i+1][j]==1) {
					adj.add("["+(i+1)+","+j+"]");
					findAdj(i+1,j, adj, matrix, checked);
				}
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
			//search position outside of matrix
		}
		try {
			//check right
			if(isNotChecked(i,j+1, checked)) {
				checked[i][j+1] = 1;
				if(matrix[i][j+1]==1) {
					adj.add("["+i+","+(j+1)+"]");
					findAdj(i,j+1, adj, matrix, checked);
				}
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
			//search position outside of matrix
		}
		try {
			//check left
			if(isNotChecked(i,j-1, checked)) {
				checked[i][j-1] = 1;
				if(matrix[i][j-1]==1) {
					adj.add("["+i+","+(j-1)+"]");
					findAdj(i,j-1, adj, matrix, checked);
				}
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
			//search position outside of matrix
		}
		return adj;
	}

	/**
	 * Checks if a position of a matrix was already visited
	 * @param i is the row number to search
	 * @param j is the column number to search
	 * @param checked boolean true if was visited before, false otherwise
	 * @return
	 */
	public static boolean isNotChecked(int i, int j, int[][] checked) {
		return (checked[i][j] == 0);
	}


	/**
	 * Read and parses a file to a two dimentional array
	 * @param filename string of the file to read and parse
	 * @return the representation of the file in a two dimensional array 
	 */
	public static int[][] readFile(String filename){
		ArrayList<int[]> matrixList = new ArrayList<int[]>();

		try {
			File file = new File("src/"+filename);
			Scanner sc = new Scanner(file);
			while(sc.hasNextLine()){
				String line = sc.nextLine();
				if(line.length()>1) {
					String[] str = line.replaceAll("\\D+","").split("");
					int[] intArray=new int[str.length];
					int i = 0;
					for(String s:str) {
						intArray[i]=Integer.parseInt(s);
						i++;
					}
					matrixList.add(intArray);
				}
			}
			sc.close();

			int[][] array2D = new int[matrixList.size()][];
			int j = 0;
			for (int[] row:matrixList) {
				array2D[j] = row;
				j++;
			}
			return array2D;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}

